
IF EXISTS (SELECT 1
           FROM sysobjects
           WHERE id = object_id('qrtz_simprop_triggers')
           AND type = 'U')
   DROP TABLE qrtz_simprop_triggers
GO
IF EXISTS (SELECT 1
           FROM sysobjects
           WHERE id = object_id('qrtz_simple_triggers')
           AND type = 'U')
   DROP TABLE qrtz_simple_triggers
GO
IF EXISTS (SELECT 1
           FROM sysobjects
           WHERE id = object_id('qrtz_scheduler_state')
           AND type = 'U')
   DROP TABLE qrtz_scheduler_state
GO
IF EXISTS (SELECT 1
           FROM sysobjects
           WHERE id = object_id('qrtz_paused_trigger_grps')
           AND type = 'U')
   DROP TABLE qrtz_paused_trigger_grps
GO
IF EXISTS (SELECT 1
           FROM sysobjects
           WHERE id = object_id('qrtz_locks')
           AND type = 'U')
   DROP TABLE qrtz_locks
GO
IF EXISTS (SELECT 1
           FROM sysobjects
           WHERE id = object_id('qrtz_fired_triggers')
           AND type = 'U')
   DROP TABLE qrtz_fired_triggers
GO
IF EXISTS (SELECT 1
           FROM sysobjects
           WHERE id = object_id('qrtz_cron_triggers')
           AND type = 'U')
   DROP TABLE qrtz_cron_triggers
GO
IF EXISTS (SELECT 1
           FROM sysobjects
           WHERE id = object_id('qrtz_calendars')
           AND type = 'U')
   DROP TABLE qrtz_calendars
GO
IF EXISTS (SELECT 1
           FROM sysobjects
           WHERE id = object_id('qrtz_blob_triggers')
           AND type = 'U')
   DROP TABLE qrtz_blob_triggers
GO
IF EXISTS (SELECT 1
           FROM sysobjects
           WHERE id = object_id('qrtz_triggers')
           AND type = 'U')
   DROP TABLE qrtz_triggers
GO
IF EXISTS (SELECT 1
           FROM sysobjects
           WHERE id = object_id('qrtz_job_details')
           AND type = 'U')
   DROP TABLE qrtz_job_details
GO
-- ----------------------------
-- 1、存储每一个已配置的 jobDetail 的详细信息
-- ----------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[qrtz_job_details](
	[sched_name] [nvarchar](120) NOT NULL,
	[job_name] [nvarchar](200) NOT NULL,
	[job_group] [nvarchar](200) NOT NULL,
	[description] [nvarchar](250) NULL,
	[job_class_name] [nvarchar](250) NOT NULL,
	[is_durable] [nvarchar](1) NOT NULL,
	[is_nonconcurrent] [nvarchar](1) NOT NULL,
	[is_update_data] [nvarchar](1) NOT NULL,
	[requests_recovery] [nvarchar](1) NOT NULL,
	[job_data] [varbinary](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[sched_name] ASC,
	[job_name] ASC,
	[job_group] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
-- 2、 存储已配置的 Trigger 的信息
-- ----------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[qrtz_triggers](
	[sched_name] [nvarchar](120) NOT NULL,
	[trigger_name] [nvarchar](200) NOT NULL,
	[trigger_group] [nvarchar](200) NOT NULL,
	[job_name] [nvarchar](200) NOT NULL,
	[job_group] [nvarchar](200) NOT NULL,
	[description] [nvarchar](250) NULL,
	[next_fire_time] [bigint] NULL,
	[prev_fire_time] [bigint] NULL,
	[priority] [int] NULL,
	[trigger_state] [nvarchar](16) NOT NULL,
	[trigger_type] [nvarchar](8) NOT NULL,
	[start_time] [bigint] NOT NULL,
	[end_time] [bigint] NULL,
	[calendar_name] [nvarchar](200) NULL,
	[misfire_instr] [smallint] NULL,
	[job_data] [varbinary](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[sched_name] ASC,
	[trigger_name] ASC,
	[trigger_group] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
-- 3、 存储简单的 Trigger，包括重复次数，间隔，以及已触发的次数
-- ----------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[qrtz_simple_triggers](
	[sched_name] [nvarchar](120) NOT NULL,
	[trigger_name] [nvarchar](200) NOT NULL,
	[trigger_group] [nvarchar](200) NOT NULL,
	[repeat_count] [bigint] NOT NULL,
	[repeat_interval] [bigint] NOT NULL,
	[times_triggered] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sched_name] ASC,
	[trigger_name] ASC,
	[trigger_group] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- ----------------------------
-- 4、 存储 Cron Trigger，包括 Cron 表达式和时区信息
-- ---------------------------- 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[qrtz_cron_triggers](
	[sched_name] [nvarchar](120) NOT NULL,
	[trigger_name] [nvarchar](200) NOT NULL,
	[trigger_group] [nvarchar](200) NOT NULL,
	[cron_expression] [nvarchar](200) NOT NULL,
	[time_zone_id] [nvarchar](80) NULL,
PRIMARY KEY CLUSTERED 
(
	[sched_name] ASC,
	[trigger_name] ASC,
	[trigger_group] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- ----------------------------
-- 5、 Trigger 作为 Blob 类型存储(用于 Quartz 用户用 JDBC 创建他们自己定制的 Trigger 类型，JobStore 并不知道如何存储实例的时候)
-- ---------------------------- 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[qrtz_blob_triggers](
	[sched_name] [nvarchar](120) NOT NULL,
	[trigger_name] [nvarchar](200) NOT NULL,
	[trigger_group] [nvarchar](200) NOT NULL,
	[blob_data] [varbinary](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[sched_name] ASC,
	[trigger_name] ASC,
	[trigger_group] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
-- 6、 以 Blob 类型存储存放日历信息， quartz可配置一个日历来指定一个时间范围
-- ---------------------------- 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[qrtz_calendars](
	[sched_name] [nvarchar](120) NOT NULL,
	[calendar_name] [nvarchar](200) NOT NULL,
	[calendar] [varbinary](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sched_name] ASC,
	[calendar_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
-- 7、 存储已暂停的 Trigger 组的信息
-- ---------------------------- 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[qrtz_paused_trigger_grps](
	[sched_name] [nvarchar](120) NOT NULL,
	[trigger_group] [nvarchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sched_name] ASC,
	[trigger_group] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- ----------------------------
-- 8、 存储与已触发的 Trigger 相关的状态信息，以及相联 Job 的执行信息
-- ---------------------------- 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[qrtz_fired_triggers](
	[sched_name] [nvarchar](120) NOT NULL,
	[entry_id] [nvarchar](95) NOT NULL,
	[trigger_name] [nvarchar](200) NOT NULL,
	[trigger_group] [nvarchar](200) NOT NULL,
	[instance_name] [nvarchar](200) NOT NULL,
	[fired_time] [bigint] NOT NULL,
	[sched_time] [bigint] NOT NULL,
	[priority] [int] NOT NULL,
	[state] [nvarchar](16) NOT NULL,
	[job_name] [nvarchar](200) NULL,
	[job_group] [nvarchar](200) NULL,
	[is_nonconcurrent] [nvarchar](1) NULL,
	[requests_recovery] [nvarchar](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[sched_name] ASC,
	[entry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- ----------------------------
-- 9、 存储少量的有关 Scheduler 的状态信息，假如是用于集群中，可以看到其他的 Scheduler 实例
-- ---------------------------- 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[qrtz_scheduler_state](
	[sched_name] [nvarchar](120) NOT NULL,
	[instance_name] [nvarchar](200) NOT NULL,
	[last_checkin_time] [bigint] NOT NULL,
	[checkin_interval] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sched_name] ASC,
	[instance_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- ----------------------------
-- 10、 存储程序的悲观锁的信息(假如使用了悲观锁)
-- ---------------------------- 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[qrtz_locks](
	[sched_name] [nvarchar](120) NOT NULL,
	[lock_name] [nvarchar](40) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sched_name] ASC,
	[lock_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- ----------------------------
-- 11、 Quartz集群实现同步机制的行锁表
-- ---------------------------- 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[qrtz_simprop_triggers](
	[sched_name] [nvarchar](120) NOT NULL,
	[trigger_name] [nvarchar](200) NOT NULL,
	[trigger_group] [nvarchar](200) NOT NULL,
	[str_prop_1] [nvarchar](512) NULL,
	[str_prop_2] [nvarchar](512) NULL,
	[str_prop_3] [nvarchar](512) NULL,
	[int_prop_1] [int] NULL,
	[int_prop_2] [int] NULL,
	[long_prop_1] [bigint] NULL,
	[long_prop_2] [bigint] NULL,
	[dec_prop_1] [decimal](13, 4) NULL,
	[dec_prop_2] [decimal](13, 4) NULL,
	[bool_prop_1] [nvarchar](1) NULL,
	[bool_prop_2] [nvarchar](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[sched_name] ASC,
	[trigger_name] ASC,
	[trigger_group] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


ALTER TABLE [dbo].[qrtz_blob_triggers]  WITH CHECK ADD  CONSTRAINT [qrtz_blob_triggers_ibfk_1] FOREIGN KEY([sched_name], [trigger_name], [trigger_group])
REFERENCES [dbo].[qrtz_triggers] ([sched_name], [trigger_name], [trigger_group])
GO
ALTER TABLE [dbo].[qrtz_blob_triggers] CHECK CONSTRAINT [qrtz_blob_triggers_ibfk_1]
GO
ALTER TABLE [dbo].[qrtz_cron_triggers]  WITH CHECK ADD  CONSTRAINT [qrtz_cron_triggers_ibfk_1] FOREIGN KEY([sched_name], [trigger_name], [trigger_group])
REFERENCES [dbo].[qrtz_triggers] ([sched_name], [trigger_name], [trigger_group])
GO
ALTER TABLE [dbo].[qrtz_cron_triggers] CHECK CONSTRAINT [qrtz_cron_triggers_ibfk_1]
GO
ALTER TABLE [dbo].[qrtz_simple_triggers]  WITH CHECK ADD  CONSTRAINT [qrtz_simple_triggers_ibfk_1] FOREIGN KEY([sched_name], [trigger_name], [trigger_group])
REFERENCES [dbo].[qrtz_triggers] ([sched_name], [trigger_name], [trigger_group])
GO
ALTER TABLE [dbo].[qrtz_simple_triggers] CHECK CONSTRAINT [qrtz_simple_triggers_ibfk_1]
GO
ALTER TABLE [dbo].[qrtz_simprop_triggers]  WITH CHECK ADD  CONSTRAINT [qrtz_simprop_triggers_ibfk_1] FOREIGN KEY([sched_name], [trigger_name], [trigger_group])
REFERENCES [dbo].[qrtz_triggers] ([sched_name], [trigger_name], [trigger_group])
GO
ALTER TABLE [dbo].[qrtz_simprop_triggers] CHECK CONSTRAINT [qrtz_simprop_triggers_ibfk_1]
GO
ALTER TABLE [dbo].[qrtz_triggers]  WITH CHECK ADD  CONSTRAINT [qrtz_triggers_ibfk_1] FOREIGN KEY([sched_name], [job_name], [job_group])
REFERENCES [dbo].[qrtz_job_details] ([sched_name], [job_name], [job_group])
GO
ALTER TABLE [dbo].[qrtz_triggers] CHECK CONSTRAINT [qrtz_triggers_ibfk_1]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调度名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_blob_triggers', @level2type=N'COLUMN',@level2name=N'sched_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'qrtz_triggers表trigger_name的外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_blob_triggers', @level2type=N'COLUMN',@level2name=N'trigger_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'qrtz_triggers表trigger_group的外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_blob_triggers', @level2type=N'COLUMN',@level2name=N'trigger_group'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'存放持久化Trigger对象' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_blob_triggers', @level2type=N'COLUMN',@level2name=N'blob_data'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blob类型的触发器表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_blob_triggers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调度名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_calendars', @level2type=N'COLUMN',@level2name=N'sched_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日历名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_calendars', @level2type=N'COLUMN',@level2name=N'calendar_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'存放持久化calendar对象' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_calendars', @level2type=N'COLUMN',@level2name=N'calendar'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日历信息表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_calendars'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调度名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_cron_triggers', @level2type=N'COLUMN',@level2name=N'sched_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'qrtz_triggers表trigger_name的外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_cron_triggers', @level2type=N'COLUMN',@level2name=N'trigger_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'qrtz_triggers表trigger_group的外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_cron_triggers', @level2type=N'COLUMN',@level2name=N'trigger_group'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'cron表达式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_cron_triggers', @level2type=N'COLUMN',@level2name=N'cron_expression'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'时区' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_cron_triggers', @level2type=N'COLUMN',@level2name=N'time_zone_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cron类型的触发器表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_cron_triggers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调度名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_fired_triggers', @level2type=N'COLUMN',@level2name=N'sched_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调度器实例id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_fired_triggers', @level2type=N'COLUMN',@level2name=N'entry_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'qrtz_triggers表trigger_name的外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_fired_triggers', @level2type=N'COLUMN',@level2name=N'trigger_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'qrtz_triggers表trigger_group的外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_fired_triggers', @level2type=N'COLUMN',@level2name=N'trigger_group'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调度器实例名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_fired_triggers', @level2type=N'COLUMN',@level2name=N'instance_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'触发的时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_fired_triggers', @level2type=N'COLUMN',@level2name=N'fired_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'定时器制定的时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_fired_triggers', @level2type=N'COLUMN',@level2name=N'sched_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优先级' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_fired_triggers', @level2type=N'COLUMN',@level2name=N'priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_fired_triggers', @level2type=N'COLUMN',@level2name=N'state'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_fired_triggers', @level2type=N'COLUMN',@level2name=N'job_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务组名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_fired_triggers', @level2type=N'COLUMN',@level2name=N'job_group'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否并发' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_fired_triggers', @level2type=N'COLUMN',@level2name=N'is_nonconcurrent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否接受恢复执行' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_fired_triggers', @level2type=N'COLUMN',@level2name=N'requests_recovery'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'已触发的触发器表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_fired_triggers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调度名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_job_details', @level2type=N'COLUMN',@level2name=N'sched_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_job_details', @level2type=N'COLUMN',@level2name=N'job_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务组名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_job_details', @level2type=N'COLUMN',@level2name=N'job_group'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'相关介绍' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_job_details', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'执行任务类名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_job_details', @level2type=N'COLUMN',@level2name=N'job_class_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否持久化' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_job_details', @level2type=N'COLUMN',@level2name=N'is_durable'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否并发' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_job_details', @level2type=N'COLUMN',@level2name=N'is_nonconcurrent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否更新数据' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_job_details', @level2type=N'COLUMN',@level2name=N'is_update_data'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否接受恢复执行' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_job_details', @level2type=N'COLUMN',@level2name=N'requests_recovery'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'存放持久化job对象' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_job_details', @level2type=N'COLUMN',@level2name=N'job_data'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务详细信息表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_job_details'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调度名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_locks', @level2type=N'COLUMN',@level2name=N'sched_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'悲观锁名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_locks', @level2type=N'COLUMN',@level2name=N'lock_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'存储的悲观锁信息表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_locks'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调度名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_paused_trigger_grps', @level2type=N'COLUMN',@level2name=N'sched_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'qrtz_triggers表trigger_group的外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_paused_trigger_grps', @level2type=N'COLUMN',@level2name=N'trigger_group'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'暂停的触发器表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_paused_trigger_grps'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调度名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_scheduler_state', @level2type=N'COLUMN',@level2name=N'sched_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实例名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_scheduler_state', @level2type=N'COLUMN',@level2name=N'instance_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上次检查时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_scheduler_state', @level2type=N'COLUMN',@level2name=N'last_checkin_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'检查间隔时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_scheduler_state', @level2type=N'COLUMN',@level2name=N'checkin_interval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调度器状态表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_scheduler_state'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调度名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simple_triggers', @level2type=N'COLUMN',@level2name=N'sched_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'qrtz_triggers表trigger_name的外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simple_triggers', @level2type=N'COLUMN',@level2name=N'trigger_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'qrtz_triggers表trigger_group的外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simple_triggers', @level2type=N'COLUMN',@level2name=N'trigger_group'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'重复的次数统计' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simple_triggers', @level2type=N'COLUMN',@level2name=N'repeat_count'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'重复的间隔时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simple_triggers', @level2type=N'COLUMN',@level2name=N'repeat_interval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'已经触发的次数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simple_triggers', @level2type=N'COLUMN',@level2name=N'times_triggered'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'简单触发器的信息表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simple_triggers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调度名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simprop_triggers', @level2type=N'COLUMN',@level2name=N'sched_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'qrtz_triggers表trigger_name的外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simprop_triggers', @level2type=N'COLUMN',@level2name=N'trigger_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'qrtz_triggers表trigger_group的外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simprop_triggers', @level2type=N'COLUMN',@level2name=N'trigger_group'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'String类型的trigger的第一个参数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simprop_triggers', @level2type=N'COLUMN',@level2name=N'str_prop_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'String类型的trigger的第二个参数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simprop_triggers', @level2type=N'COLUMN',@level2name=N'str_prop_2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'String类型的trigger的第三个参数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simprop_triggers', @level2type=N'COLUMN',@level2name=N'str_prop_3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'int类型的trigger的第一个参数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simprop_triggers', @level2type=N'COLUMN',@level2name=N'int_prop_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'int类型的trigger的第二个参数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simprop_triggers', @level2type=N'COLUMN',@level2name=N'int_prop_2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'long类型的trigger的第一个参数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simprop_triggers', @level2type=N'COLUMN',@level2name=N'long_prop_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'long类型的trigger的第二个参数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simprop_triggers', @level2type=N'COLUMN',@level2name=N'long_prop_2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'decimal类型的trigger的第一个参数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simprop_triggers', @level2type=N'COLUMN',@level2name=N'dec_prop_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'decimal类型的trigger的第二个参数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simprop_triggers', @level2type=N'COLUMN',@level2name=N'dec_prop_2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Boolean类型的trigger的第一个参数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simprop_triggers', @level2type=N'COLUMN',@level2name=N'bool_prop_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Boolean类型的trigger的第二个参数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simprop_triggers', @level2type=N'COLUMN',@level2name=N'bool_prop_2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'同步机制的行锁表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_simprop_triggers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调度名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'sched_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'触发器的名字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'trigger_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'触发器所属组的名字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'trigger_group'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'qrtz_job_details表job_name的外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'job_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'qrtz_job_details表job_group的外键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'job_group'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'相关介绍' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上一次触发时间（毫秒）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'next_fire_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'下一次触发时间（默认为-1表示不触发）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'prev_fire_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优先级' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'触发器状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'trigger_state'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'触发器的类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'trigger_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开始时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'start_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'结束时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'end_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日程表名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'calendar_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'补偿执行的策略' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'misfire_instr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'存放持久化job对象' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers', @level2type=N'COLUMN',@level2name=N'job_data'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'触发器详细信息表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'qrtz_triggers'
GO
